### 图片未显示

如果src的路径为变量，请检查src是否少了冒号。

```
 <img :src="setting.logo">
 或者
 <img src="../../public/logo.png">
```

